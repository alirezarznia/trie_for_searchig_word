#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	piii 		pair<ll,pii>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62
typedef long long ll;
using namespace std;
ll arr[101][26];
bool bl[101];
ll F(string s){
    ll q =0;
    while(q != s.size()){
        if(arr[q][s[q]-'a']==1) q++;
        else return cout<<"0"<<endl , 0;
    }
    if(bl[q-1])
        return cout<<"1"<<endl , 0;
    else return cout<<"0"<<endl , 0;
}
int main(){
 //   Test;
    ll t; cin >> t;
    while(t--){
        Set(bl , false);
        Set(arr , -1);ll n ; cin>>n;
        Rep(i ,n){
        string str; cin >>str;
        Rep(i , str.size()) arr[i][str[i]-'a']=1;
        bl[str.size()-1]=true;
        }
        string y ; cin>>y;
        F(y);
    }
    return 0;
}
